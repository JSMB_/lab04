package linearalgebra;

import static org.junit.Assert.*;
import org.junit.Test;

import linearalgebra.Vector3d.*;

public class Vector3dTests {
    
    //Joel S Molina B   22235364
    @Test
    public void vector3dCreation() {
        Vector3d myV = new Vector3d(4, 2, 7);
        assertEquals("get x", 4, myV.getX(), 0.10);
        assertEquals("get y", 2, myV.getY(), 0.10);
        assertEquals("get z", 7, myV.getZ(), 0.10);
    }

    @Test
     public void testmagnitude() {
        Vector3d myV = new Vector3d(1, 1, 1);
        assertEquals("testing if magnitude works as expected" , 1.73, myV.magnitude(), 0.10);
    }

    @Test
    public void testDotProduct() {
        Vector3d myV = new Vector3d(3, 5, 2);
        Vector3d otherV = new Vector3d(3, 4, 2);
        assertEquals("testing if the end double works", myV.dotProduct(otherV), 33, 0.10);
    }

    @Test
    public void testAdd() {
        Vector3d myV = new Vector3d(2, 1, 4);
        Vector3d otherV = new Vector3d(4, 2, 1);
        Vector3d otherV2 = new Vector3d(6, 3, 5);
        assertTrue(otherV2.equals(myV.add(otherV)));
    }

    @Test
    public void testGetX() {
        Vector3d myV = new Vector3d(2, 1, 4);
        assertTrue(myV.getX() == 2);
    }

    @Test
    public void testGetY() {
        Vector3d myV = new Vector3d(2, 1, 4);
        assertTrue(myV.getY() == 1);
    }

    @Test
    public void testGetZ() {
        Vector3d myV = new Vector3d(2, 1, 4);
        assertTrue(myV.getZ() == 4);
    }
}
