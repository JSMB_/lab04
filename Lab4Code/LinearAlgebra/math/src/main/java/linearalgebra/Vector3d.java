package linearalgebra;

public class Vector3d {
    protected double x;
    protected double y;
    protected double z;
    //Joel S Molina B   2235364
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public String toString() {
        return "X: " + this.x + "\nY: " + this.y + "\nZ: " + this.z;
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public double dotProduct(Vector3d otherV) {
        double result = otherV.x * this.x + otherV.y * this.y + otherV.z * this.z;
        return result;
    }

    public Vector3d add(Vector3d otherV) {
        otherV.x += this.x;
        otherV.y += this.y;
        otherV.z += this.z;
        return otherV;
    }

    public boolean equals(Object o) {
        Vector3d e = (Vector3d) o;
        if(e.x == this.x && e.y == this.y && e.z == this.z) {
           return true;
        }
        return false;
    }
}
